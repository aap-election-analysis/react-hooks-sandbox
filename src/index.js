import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import ReactDOM from "react-dom";

import "./styles.css";

function useAsyncHook(searchBook) {
    const [result, setResult] = React.useState([]);
    const [loading, setLoading] = React.useState("false");

    React.useEffect(() => {
        async function fetchBookList() {
            try {
                setLoading("true");
                const response = await fetch(
                    `https://www.googleapis.com/books/v1/volumes?q=${searchBook}&key=AIzaSyAu-8Jn5Oban8usb67efHChHzZ7d-Xyyxo&projection=full`
                );

                const json = await response.json();

                console.log(json);


                setResult(
                    json.items.map(item => {
                        item.volumeInfo.id = item.id
                        return item.volumeInfo;
                    })
                );
            } catch (error) {
                setLoading("null");
            }
        }

        if (searchBook !== "") {
            fetchBookList();
        }
    }, [searchBook]);

    return [result, loading];
}

function App() {
    const [search, setSearch] = React.useState("");
    const [query, setQuery] = React.useState("");
    const [result, loading] = useAsyncHook(query);
    return (
        <div className="App">
            <Router>
                <h1>Search for Books on any Topic</h1>
                <form
                    onSubmit={e => {
                        e.preventDefault();
                        setQuery(search);
                    }}
                >
                    <label>Search : </label>
                    <input type="text" onChange={e => setSearch(e.target.value)} />
                    <input type="submit" value="search" />
                </form>

                {loading === "false" ? (
                    <h1>Search for Books</h1>
                ) : loading === "null" ? (
                    <h1>No Book Found</h1>
                ) : (
                    result.map(item => {
                        //console.log(item.id);
                        return <Link to={`/book/${item.id}`}>Users<p>Book Title : {item.title}</p></Link>;
                    })
                )}
            </Router>
        </div>
    );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);